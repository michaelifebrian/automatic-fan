#define tempSensorPin A0
const int motor1PinForward = 3;
const int motor1PinBackward = 5;
const int motor2PinForward = 10;
const int motor2PinBackward = 11;
const int enablePinMotor1 = 7;
const int enablePinMotor2 = 8;
const int blueLED = 9;
const int redLED = 6;
int temp = 0;
int blueColor = 0;
int redColor = 0;
unsigned long previousMillis = 0;
int ledState = LOW;  


void setup() {
    pinMode(motor1PinForward, OUTPUT);
    pinMode(motor1PinBackward, OUTPUT);
    pinMode(motor2PinForward, OUTPUT);
    pinMode(motor2PinBackward, OUTPUT);
    pinMode(enablePinMotor1, OUTPUT);
    pinMode(enablePinMotor2, OUTPUT);
    pinMode(tempSensorPin,INPUT);
    pinMode(blueLED,OUTPUT);
    pinMode(redLED,OUTPUT);

    digitalWrite(enablePinMotor1, HIGH);
    digitalWrite(enablePinMotor2, HIGH);
}

void loop() {
    unsigned long currentMillis = millis();
    temp = map(analogRead(tempSensorPin),20,358,-40,125); //mapping value analog menjadi nilai temperature
  
    if(temp < 20){
      turnOnFan(70); //nyalain kipas speed 70
    } else if(temp > 20 && temp < 25){
      turnOnFan(150); //nyalain kipas speed 150
    } else if(temp > 25){
      turnOnFan(255); //nyalain kipas speed full/255
    }
  
    if(temp > 0 && temp < 16){
      blueColor = map(temp,0,16,255,0); //mapping temperature 0-16 jadi analog 255-0 utk led
    } else if (temp < 0){
      blueColor = 255;
    } else if (temp > 16){
      blueColor = 0;
    }
  
    if(temp > 16 && temp < 32){
      redColor = map(temp,16,32,255,0); //mapping temperature 16-32 jadi analog 255-0 utk pwm
    } else if (temp < 16){
      redColor = 0;
    } else if (temp > 32){
      redColor = 255;
    }
  
    if(temp > 32){ //keadaan panas
      if(currentMillis - previousMillis >= 300) { //fungsi blinking dg millis
        previousMillis = currentMillis;
            ledState = !ledState;
            digitalWrite(redLED,ledState); 
      }
    } else if(temp < 32){ //keadaan normal
        analogWrite(redLED,redColor); //nyalain led sesuai levelnya
        analogWrite(blueLED,blueColor); //nyalain led sesuai levelnya
    }
   delay(50);
}


void turnOnFan(int v){ //fungsi driver motor
  analogWrite(motor1PinForward,v);
  analogWrite(motor1PinBackward,0);
  analogWrite(motor2PinForward,v);
  analogWrite(motor2PinBackward,0);
}
