# Automatic Fan



## Fungsi

2 Dinamo yang terintegrasi akan memiliki kecepatan yang bergantung pada suhu di sensor TMP36. Kemudian terdapat indikator LED sebagai indikator suhu sederhana.

## Komponen yang digunakan

- Arduino Board
- L293D H-Bridge IC
- TMP36

## Simulasi program
https://www.tinkercad.com/things/h61r712Y2iV-microproject-heroes/editel?sharecode=5fqwAHoQGvnvLQscGBVNJkVkU9XnU1PXVgS-KE4O0L0
